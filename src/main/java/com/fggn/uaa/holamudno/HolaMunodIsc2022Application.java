package com.fggn.uaa.holamudno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HolaMunodIsc2022Application {

	public static void main(String[] args) {
		SpringApplication.run(HolaMunodIsc2022Application.class, args);
	}

}
