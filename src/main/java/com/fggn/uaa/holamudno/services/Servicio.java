package com.fggn.uaa.holamudno.services;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Servicio {
	
	@GetMapping("/")
	public String index() {
		return "Get from Spring Boot!";
	}
	

	@GetMapping("/hola")
	public String holaGet() {
		return "Get from Spring Boot!";
	}
	
	@PostMapping("/hola")
	public String holaPost() {
		return "Post from Spring Boot!";
	}
	

}
